<?php

namespace App\Http\Controllers;
use App\Review;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Resources\ProductResource;
class ProductController extends Controller
{
     // // public function __construct()
     // {
     //
     //   $this ->middleware('auth:api');
     // }









    /**
     * Display a listing of the resource.
     *
      * @return \Illuminate\Http\Response
     */



     public function index()
     {
       return (Product::all());
    }

    //
    // /**
     // * Store a newly created resource in storage.
     // *
     // * @param  \Illuminate\Http\Request  $request
     // * @return \Illuminate\Http\Response
     // */
    public function store(Request $request)
    {
        return (Product::create($request->all()));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //return (Product::where('id','=',$product)->first());
        return new ProductResource($product);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
       $product->update($request->all());

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
      $product->review()->delete();
        $product->delete();

}
}
