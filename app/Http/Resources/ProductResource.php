<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
             'name'=>$this->name,
             'actual_price'=>'Rs'.$this->price,
             'discount_price'=>'Rs'.(1-$this->discount/100)*$this->price,
             'stock'=> $this->isstock($this->stock),

              // $this->stock > 0 ? $this->stock ." ".'(sufficient stock)':'out of stock',
                          'href' =>
           [
             'reviews'=>route('reviews.index',$this->id),
           ]
        ];
    }
    public function isstock($stockl){
    if ($stockl>0){
      return $stockl;
    }else{
      return 'out of stock';
    }
    }
}
