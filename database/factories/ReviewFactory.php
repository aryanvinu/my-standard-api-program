<?php

use Faker\Generator as Faker;
use App\Product;
$factory->define(App\Review::class, function (Faker $faker) {
    return [
        'review'=> $faker->realText(100),
        'customer'=>$faker->name,
        'star'=>$faker->numberBetween(1,5),
        'product_id'=> function(){
          return Product::all()->random();
        }
    ];
});
